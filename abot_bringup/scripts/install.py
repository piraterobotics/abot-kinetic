#!/usr/bin/env python

import sys
import os
import platform

robot_name = 'abot'

print("Installing " + robot_name + " ...\n")
print("--- INFO ---")
print("System: " + str(platform.uname()))
print("Platform: " + platform.platform())
print("Processor: " + platform.processor() + "\n")

#####################################
# GET udev's target folder
#####################################
if(os.path.isdir("/etc/udev/rules.d")):
	target_folder = "/etc/udev/rules.d"
else:
	print("Error: folder /etc/udev/rules.d not found, exiting!")
	exit()


#####################################
# GET robot's udev source folder
#####################################
print("--- HARDWARE ARCHITECTURE ---")
if(platform.processor().startswith("x86")):
	print("Detected: x86")
	source_folder = 'x86'
elif(platform.processor().startswith("i686")):
	print("Detected: i686")
	source_folder = 'x86'
elif(platform.processor().startswith("arm")):
	print("Detected: arm")
	source_folder = 'arm'
elif(platform.processor().startswith("aarch64")):
	print("Detected: aarch64")
	source_folder = 'arm'


#####################################
# INSTALL udev rules
#####################################
copy_cmd = "sudo cp $(rospack find " + robot_name + "_bringup)/udev/" + source_folder + "/* " + target_folder
udev_cmd = "sudo udevadm control --reload-rules && sudo service udev restart && sudo udevadm trigger"
print("Copying udev-files to " + target_folder)
os.system(copy_cmd)
