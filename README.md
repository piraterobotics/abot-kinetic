# ROS Example Robot 1

Example ROS-launchers for use in a terminal.

1. Start Gazebo Simulation + RViz:
   This launches gazebo and RViz, use 2D Nav Goal in RViz to start creating a map. Save map with `rosrun map_server map_saver -f yourmap` if you've finished.

   `roslaunch abot_gazebo abot_world.launch`

2. Start RViz only:
   Use RViz only for Navigation and map creation.

   `roslaunch abot_description abot_description.launch`



