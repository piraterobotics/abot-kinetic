#!/usr/bin/env python

import rospy
from rospy.client import init_node
from opencv_apps.msg import CircleArrayStamped
from std_msgs.msg import Float64
from sensor_msgs.msg import JointState
from math import pi
import time

class BallDetect():
  def __init__(self):
    rospy.on_shutdown(self.shutdown)
    self.TILT_MOTOR_ID = 0
    self.TILT_MOTOR_MIN = 0
    self.TILT_MOTOR_MAX = 0
    self.CAM_RESOLUTION_HEIGHT = 0
    self.CIRCLE_SAMPLE_THRESHOLD = 8
    self.CIRCLE_SAMPLE_TIMEOUT = 0.4
    self.SEQUENCE_THRESHOLD = 5
    self.TIMEOUT = 0.5
    self.freq = 10
    self.rate = rospy.Rate(self.freq)
    self.activated = False
    self.circles_count = 0
    self.circle_count = []
    self.circle_sequence = 0
    self.last_time = time.time()
    self.last_time_triggered = False
    self.current_motor_pos = 0.0

    self.circles_sub = rospy.Subscriber('hough_circles/circles', CircleArrayStamped, self.circles_cb)
    self.joint_states_sub = rospy.Subscriber('/joint_states', JointState, self.joint_states_cb)
    self.head_tilt_pub = rospy.Publisher('head_tilt_motor/command', Float64, queue_size=1)
    self.CAM_RESOLUTION_HEIGHT = rospy.get_param('/quick_cam/height')
    self.TILT_MOTOR_MIN = rospy.get_param('/arbotix/joints/head_tilt_motor/min_angle') * (pi / 180)
    self.TILT_MOTOR_MAX = rospy.get_param('/arbotix/joints/head_tilt_motor/max_angle') * (pi / 180)

  def circles_cb(self, msg):
    if not self.activated:
      try:
        if len(msg.circles) > 0:
          if not self.last_time_triggered:
            self.last_time = time.time()
            self.last_time_triggered = True
          else:
            print("ACTIVATOR observing " + str(self.circles_count))
            self.circles_count += 1
            elapsed_time = time.time() - self.last_time
            if (elapsed_time > self.CIRCLE_SAMPLE_TIMEOUT) and (self.circles_count > self.CIRCLE_SAMPLE_THRESHOLD):
              print("ACTIVATOR triggered, activated now!")
              self.activated = True
              self.last_time = time.time()
      except:
        pass
    else:
      if len(msg.circles) > 0:
        try:
          print("SEQUENCE: " + str(msg.header.seq))
          print("CIRCLES: " + str(self.circle_count))
          if len(self.circle_count) == 0:
            circle = msg.circles[0].center.y
            self.circle_count.append(circle)
            self.circle_sequence = msg.header.seq
          else:
            diff = msg.header.seq - (self.circle_sequence + len(self.circle_count))
            print("DIFF: " + str(diff))
            circle = msg.circles[0].center.y
            print("CIRCLE: " + str(circle))
            self.circle_count.append(circle)
            if len(self.circle_count) > self.SEQUENCE_THRESHOLD:
              self.publish()
              self.reset()
        except:
          pass

  def joint_states_cb(self, msg):
    self.current_motor_pos = msg.position[self.TILT_MOTOR_ID]

  def reset(self):
    self.activated = False
    self.last_time_triggered = False
    self.circle_count = []

  def publish(self):
    offset = (self.CAM_RESOLUTION_HEIGHT / 2) - (sum(self.circle_count) / len(self.circle_count))
    if offset < 0:
      print("offset negative")
      if (self.current_motor_pos + 0.1) < self.TILT_MOTOR_MAX:
        print("look down!")
        msg = Float64()
        msg.data = self.current_motor_pos + 0.1
        self.head_tilt_pub.publish(msg)
    else:
      print("offset positive")
      if (self.current_motor_pos - 0.1) > self.TILT_MOTOR_MIN:
        print("look up!")
        msg = Float64()
        msg.data = self.current_motor_pos - 0.1
        self.head_tilt_pub.publish(msg)

  def loop(self):
    while not rospy.is_shutdown():
      self.rate.sleep()

  def shutdown(self):
    print("Ok, shutting down")

if __name__ == '__main__':
  try:
    init_node("ball_detect", log_level=rospy.INFO)
    bd=BallDetect()
    bd.loop()
  except rospy.ROSInterruptException:
    print("Bye-bye...")
