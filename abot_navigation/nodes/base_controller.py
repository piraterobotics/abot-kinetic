#!/usr/bin/env python

import roslib;
import rospy
import os
from math import pi, sin, cos
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist, Quaternion, Pose
from tf.broadcaster import TransformBroadcaster

from rospy.client import init_node
import geometry_msgs
from geometry_msgs.msg._TransformStamped import TransformStamped
from std_msgs.msg._Float32MultiArray import Float32MultiArray
import std_srvs.srv

class BaseController():
    def __init__(self):
      self.rate = float(rospy.get_param('~base_controller_rate', 10))

      # **************
      # Odometry Setup
      # **************
      #self.base_frame_id = rospy.get_param('~base_frame_id', 'chassis')
      self.base_frame_id = rospy.get_param('~base_frame_id', 'footprint')
      self.odom_frame_id = rospy.get_param('~odom_frame_id', 'odom')
      self.wheels_frame_id = rospy.get_param('~wheels_frame_id', 'chassis')
      self.encoder_right = 0.0           # encoder1 topic data
      self.encoder_left = 0.0            # encoder2 topic data
      self.wheels_track = 0.26           # Track = distance between both wheels starting from the middle of rubber tread (26mm).
      self.wheel_diameter = 0.1          # 100mm
      self.wheel_ticks = 360             # Ticks per revolution
      self.last_encoder_left = 0.0       # previous encoder left
      self.last_encoder_right = 0.0      # previous encoder right
      self.x = 0.0                       # x coordinate result
      self.y = 0.0                       # y coordinate result
      self.th = 0.0                      # angle direction result
      self.last_time = rospy.Time.now()  # used in time delta calculation
      self.ticks_per_meter = 1126

      # **************
      # Publisher
      # **************
      self.odom_pub = rospy.Publisher('odom', Odometry, queue_size=10)
      self.odom_broadcaster = TransformBroadcaster()

      # **************
      # Subscriber
      # **************
      rospy.Subscriber('pirate_md25_encoders', Float32MultiArray, self.encoders_cb )

      # **************
      # Service
      # **************
      self.reset_service = rospy.Service('pirate_odom_reset', std_srvs.srv.Empty, self.odom_reset_cb)

    def loop(self):
      r = rospy.Rate(self.rate)
      while not rospy.is_shutdown():
        self.update()
        r.sleep()

    def update(self):
      timestamp = rospy.Time.now()         # Current time used for time delta, self.last_time is set in the end
      delta_left = 0.0                     # Distance travelled on left wheel
      delta_right = 0.0                    # Distance travelled on right wheel
      delta_xy = 0.0                       # Incremental delta x,y
      delta_th = 0.0                       # Incremental delta theta
      vxy = 0.0                            # Velocity x,y
      vth = 0.0                            # Velocity theta
      dx = 0.0                             # Distance travelled in x
      dy = 0.0                             # Distance travelled in y

      # Time delta
      dt = timestamp - self.last_time
      dt = dt.to_sec()

      # Distance travelled on each wheel
      delta_left = (self.encoder_left - self.last_encoder_left) / self.ticks_per_meter
      delta_right = (self.encoder_right - self.last_encoder_right) / self.ticks_per_meter

      # Save current encoder values to memory
      self.last_encoder_left = self.encoder_left;
      self.last_encoder_right = self.encoder_right;

      # Incremental deltas of x,y & theta
      delta_xy = ( delta_right + delta_left ) * 0.5;
      delta_th = ( delta_right - delta_left ) / self.wheels_track;
      vxy = delta_xy / dt;
      vth = delta_th / dt;

      if( delta_th != 0 ):
        self.th += delta_th

      # Read(casesensitive): rossum.sourgeforge.net/papers/DiffSteer
      if( delta_xy != 0 ):
        dx = cos(delta_th) * delta_xy
        dy = -sin(delta_th) * delta_xy
        self.x += (cos(self.th) * dx - sin(self.th) * dy)
        self.y += (sin(self.th) * dx + cos(self.th) * dy)

      # Quaternion direction is the rotation of our robot
      quaternion = Quaternion()
      quaternion.x = 0.0
      quaternion.y = 0.0
      quaternion.z = sin( self.th * 0.5 )
      quaternion.w = cos( self.th * 0.5 )

      # Odometry transform data
      odom_trans = TransformStamped()
      odom_trans.header.stamp = timestamp
      odom_trans.header.frame_id = self.odom_frame_id
      odom_trans.child_frame_id = self.base_frame_id
      odom_trans.transform.translation.x = self.x
      odom_trans.transform.translation.y = self.y
      odom_trans.transform.translation.z = 0.0
      odom_trans.transform.rotation = quaternion;

      # Publish tf
      self.odom_broadcaster.sendTransformMessage(odom_trans)

      odom = Odometry()
      odom.header.frame_id = self.odom_frame_id
      odom.child_frame_id = self.base_frame_id
      odom.header.stamp = timestamp
      odom.pose.pose.position.x = self.x
      odom.pose.pose.position.y = self.y
      odom.pose.pose.position.z = 0.0
      odom.pose.pose.orientation = quaternion
      odom.twist.twist.linear.x = vxy
      odom.twist.twist.linear.y = 0.0
      odom.twist.twist.angular.z = vth

      # Publish odom
      self.odom_pub.publish(odom)

      # Time delta calculation at top of update()
      self.last_time = timestamp

    # All calculations here depend on those two values
    def encoders_cb(self, msg):
      self.encoder_right = msg.data[0]
      self.encoder_left = msg.data[1]

    # Reset odometry on user demand, robot itself would never do that.
    def odom_reset_cb(self, req):
        self.x = 0.0
        self.y = 0.0
        self.th = 0.0
        return std_srvs.srv.EmptyResponse()

if __name__ == '__main__':
    try:
      init_node("base_controller", log_level=rospy.INFO)
      bc = BaseController()
      bc.loop()
    except rospy.ROSInterruptException:
      bc.reset_service.shutdown("shutdown requested")
